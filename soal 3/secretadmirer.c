#define FUSE_USE_VERSION 30

#include <fuse.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>
#include <openssl/bio.h>
#include <openssl/evp.h>

#define ENCODED_PREFIX "encoded_"
#define DECODED_PREFIX "decoded_"

static const char *source_dir = "/home/users/dire";
static const char *password = "userf07"; 

// Fungsi untuk melakukan encode Base64
char* base64_encode(const unsigned char* input, int length) {
    BIO *bio, *b64;
    BUF_MEM *bufferPtr;
    char *buffer;

    b64 = BIO_new(BIO_f_base64());
    bio = BIO_new(BIO_s_mem());
    bio = BIO_push(b64, bio);

    BIO_set_flags(bio, BIO_FLAGS_BASE64_NO_NL);
    BIO_write(bio, input, length);
    BIO_flush(bio);
    BIO_get_mem_ptr(bio, &bufferPtr);

    buffer = (char *)malloc(bufferPtr->length + 1);
    memcpy(buffer, bufferPtr->data, bufferPtr->length);
    buffer[bufferPtr->length] = '\0';

    BIO_free_all(bio);
    return buffer;
}

// Fungsi untuk melakukan decode Base64
unsigned char* base64_decode(const char* input, int length, int* out_length) {
    BIO *bio, *b64;
    unsigned char* buffer = (unsigned char *)malloc(length);
    memset(buffer, 0, length);

    bio = BIO_new_mem_buf(input, length);
    b64 = BIO_new(BIO_f_base64());
    bio = BIO_push(b64, bio);

    BIO_set_flags(bio, BIO_FLAGS_BASE64_NO_NL);
    *out_length = BIO_read(bio, buffer, length);

    BIO_free_all(bio);
    return buffer;
}

// Fungsi untuk melakukan encode nama file/direktori jika memenuhi kriteria
void encode_name(char *name, int is_directory) {
    int length = strlen(name);
    if (length <= 4) {
        // Mengubah nama menggunakan data format binary dari ASCII code masing-masing karakter
        for (int i = 0; i < length; i++) {
            name[i] = (char)(((int)name[i]) & 0xFF);
        }
    } else {
        // Menggunakan Base64 untuk mengencode nama file/direktori
        char *encoded_name = base64_encode((const unsigned char *)name, length);
        if (is_directory) {
            sprintf(name, "%s%s", DECODED_PREFIX, encoded_name);
        } else {
            sprintf(name, "%s%s", ENCODED_PREFIX, encoded_name);
        }
        free(encoded_name);
    }
}

// Implementasi fungsi-fungsi FUSE
static int my_getattr(const char *path, struct stat *stbuf) {
    int res;
    char fullpath[PATH_MAX];

    // Membuat path absolut untuk direktori/file
    sprintf(fullpath, "%s%s", source_dir, path);

    // Mendapatkan atribut dari direktori/file menggunakan fungsi stat()
    res = lstat(fullpath, stbuf);
    if (res == -1)
        return -errno;

    return 0;
}

static int my_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi) {
    DIR *dp;
    struct dirent *de;
    char fullpath[PATH_MAX];

    // Membuka direktori menggunakan fungsi opendir()
    sprintf(fullpath, "%s%s", source_dir, path);
    dp = opendir(fullpath);
    if (dp == NULL)
        return -errno;

    // Membaca setiap file/direktori dalam direktori menggunakan fungsi readdir()
    while ((de = readdir(dp)) != NULL) {
        struct stat st;
        memset(&st, 0, sizeof(st));

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;

        // Mengabaikan file/direktori yang diawali dengan huruf L, U, T, atau H
        if (de->d_name[0] == 'L' || de->d_name[0] == 'U' || de->d_name[0] == 'T' || de->d_name[0] == 'H')
            continue;

        // Mengencode nama file/direktori jika memenuhi kriteria
        encode_name(de->d_name, de->d_type == DT_DIR);

        // Menambahkan file/direktori ke dalam buffer menggunakan filler
        if (filler(buf, de->d_name, &st, 0))
            break;
    }

    // Menutup direktori menggunakan fungsi closedir()
    closedir(dp);
    return 0;
}

static int my_open(const char *path, struct fuse_file_info *fi) {
    int res;
    char fullpath[PATH_MAX];

    // Membuat path absolut untuk file
    sprintf(fullpath, "%s%s", source_dir, path);

    // Membuka file menggunakan fungsi open()
    res = open(fullpath, fi->flags);
    if (res == -1)
        return -errno;

    fi->fh = res;
    return 0;
}

static int my_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi) {
    int res;

    // Membaca file menggunakan fungsi pread()
    res = pread(fi->fh, buf, size, offset);
    if (res == -1)
        res = -errno;

    return res;
}

static int my_mkdir(const char *path, mode_t mode) {
    int res;
    char fullpath[PATH_MAX];

    // Membuat path absolut untuk direktori
    sprintf(fullpath, "%s%s", source_dir, path);

    // Membuat direktori menggunakan fungsi mkdir()
    res = mkdir(fullpath, mode);
    if (res == -1)
        return -errno;

    return 0;
}

static int my_create(const char *path, mode_t mode, struct fuse_file_info *fi) {
    int res;
    char fullpath[PATH_MAX];

    // Membuat path absolut untuk file
    sprintf(fullpath, "%s%s", source_dir, path);

    // Membuat file menggunakan fungsi creat()
    res = creat(fullpath, mode);
    if (res == -1)
        return -errno;

    fi->fh = res;
    return 0;
}

static int my_write(const char *path, const char *buf, size_t size, off_t offset, struct fuse_file_info *fi) {
    int res;

    // Menulis ke file menggunakan fungsi pwrite()
    res = pwrite(fi->fh, buf, size, offset);
    if (res == -1)
        res = -errno;

    return res;
}

static int my_rename(const char *oldpath, const char *newpath) {
    int res;
    char old_fullpath[PATH_MAX];
    char new_fullpath[PATH_MAX];

    // Membuat path absolut untuk file yang akan direname dan tujuan rename
    sprintf(old_fullpath, "%s%s", source_dir, oldpath);
    sprintf(new_fullpath, "%s%s", source_dir, newpath);

    // Melakukan rename menggunakan fungsi rename()
    res = rename(old_fullpath, new_fullpath);
    if (res == -1)
        return -errno;

    return 0;
}

static int my_unlink(const char *path) {
    int res;
    char fullpath[PATH_MAX];

    // Membuat path absolut untuk file yang akan dihapus
    sprintf(fullpath, "%s%s", source_dir, path);

    // Menghapus file menggunakan fungsi unlink()
    res = unlink(fullpath);
    if (res == -1)
        return -errno;

    return 0;
}

static int my_rmdir(const char *path) {
    int res;
    char fullpath[PATH_MAX];

    // Membuat path absolut untuk direktori yang akan dihapus
    sprintf(fullpath, "%s%s", source_dir, path);

    // Menghapus direktori menggunakan fungsi rmdir()
    res = rmdir(fullpath);
    if (res == -1)
        return -errno;

    return 0;
}

static int my_truncate(const char *path, off_t size) {
    int res;
    char fullpath[PATH_MAX];

    // Membuat path absolut untuk file yang akan di-truncate
    sprintf(fullpath, "%s%s", source_dir, path);

    // Melakukan truncate menggunakan fungsi truncate()
    res = truncate(fullpath, size);
    if (res == -1)
        return -errno;

    return 0;
}

static int my_chmod(const char *path, mode_t mode) {
    int res;
    char fullpath[PATH_MAX];

    // Membuat path absolut untuk file/direktori yang akan diubah permissionnya
    sprintf(fullpath, "%s%s", source_dir, path);

    // Mengubah permission menggunakan fungsi chmod()
    res = chmod(fullpath, mode);
    if (res == -1)
        return -errno;

    return 0;
}

static int my_chown(const char *path, uid_t uid, gid_t gid) {
    int res;
    char fullpath[PATH_MAX];

    // Membuat path absolut untuk file/direktori yang akan diubah owner/group-nya
    sprintf(fullpath, "%s%s", source_dir, path);

    // Mengubah owner/group menggunakan fungsi chown()
    res = chown(fullpath, uid, gid);
    if (res == -1)
        return -errno;

    return 0;
}


int main(int argc, char *argv[]) {
    // Inisialisasi FUSE dan konfigurasi opsinya
    struct fuse_operations operations = {
        .getattr = my_getattr,
        .readdir = my_readdir,
        .open = my_open,
        .read = my_read,
        .mkdir = my_mkdir,
        .create = my_create,
        .write = my_write,
        .rename = my_rename,
        .unlink = my_unlink,
        .rmdir = my_rmdir,
        .truncate = my_truncate,
        .chmod = my_chmod,
        .chown = my_chown,
    };

    return fuse_main(argc, argv, &operations, NULL);
}
