#define FUSE_USE_VERSION 31

#include <fuse.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

static const char *zip_path = "/home/arkandarvesh/zip/nanaxgerma.zip";  // Path to the ZIP file
static const char *log_file = "/home/arkandarvesh/logmucatatsini.txt";  // Path to the log file
static const char *restricted_folder = "/home/arkandarvesh/src_data/germa/projects/restricted_list";
static const char *bypass_folder = "/home/arkandarvesh/src_data/germa/projects/bypass_list";
static const char *restricted_file = "/home/arkandarvesh/src_data/germa/projects/restricted_list/filePenting";
static const char *restricted_file_lama = "/home/arkandarvesh/src_data/germa/projects/restricted_list/restrictedFileLama";
static const char *bypass_keyword = "bypass";

static int is_restricted(const char *path) {
    // Check if the path contains the word "restricted"
    return (strstr(path, "restricted") != NULL);
}

static int is_bypass_allowed(const char *path) {
    // Check if the path contains the bypass keyword
    return (strstr(path, bypass_keyword) != NULL);
}

static void log_action(const char *status, const char *cmd, const char *desc) {
    // Get the current time
    time_t t = time(NULL);
    struct tm tm = *localtime(&t);

    // Open the log file in append mode
    FILE *file = fopen(log_file, "a");
    if (file == NULL) {
        perror("Failed to open log file");
        return;
    }

    // Write the log entry to the file
    fprintf(file, "%s::%02d/%02d/%04d-%02d:%02d:%02d::%s::%s\n",
            status,
            tm.tm_mday, tm.tm_mon + 1, tm.tm_year + 1900,
            tm.tm_hour, tm.tm_min, tm.tm_sec,
            cmd, desc);

    // Close the log file
    fclose(file);
}

static int germa_getattr(const char *path, struct stat *stbuf) {
    // Check if the path is restricted
    if (is_restricted(path) && !is_bypass_allowed(path)) {
        return -ENOENT;
    }

    // Set the permissions and other attributes of the file/directory
    memset(stbuf, 0, sizeof(struct stat));
    if (strcmp(path, "/") == 0) {
        // Root directory
        stbuf->st_mode = S_IFDIR | 0755;
        stbuf->st_nlink = 2;
    } else {
        // Regular file or directory
        stbuf->st_mode = S_IFREG | 0644;
        stbuf->st_nlink = 1;
        stbuf->st_size = 0;
    }

    return 0;
}

static int germa_readdir(const char *path, void *buf, fuse_fill_dir_t filler,
                         off_t offset, struct fuse_file_info *fi) {
    // Check if the path is restricted
    if (is_restricted(path) && !is_bypass_allowed(path)) {
        return -ENOENT;
    }

    // Add the entries to the directory listing
    filler(buf, ".", NULL, 0);
    filler(buf, "..", NULL, 0);
    filler(buf, "file1.txt", NULL, 0);
    filler(buf, "file2.txt", NULL, 0);
    filler(buf, "file3.txt", NULL, 0);

    return 0;
}

static int germa_mkdir(const char *path, mode_t mode) {
    // Check if the path is restricted
    if (is_restricted(path) && !is_bypass_allowed(path)) {
        log_action("FAILED", "MKDIR", path);
        return -ENOENT;
    }

    // Create the directory
    int res = mkdir(path, mode);
    if (res == 0) {
        log_action("SUCCESS", "MKDIR", path);
    } else {
        log_action("FAILED", "MKDIR", path);
    }

    return res;
}

static int germa_rename(const char *from, const char *to) {
    // Check if the source or destination paths are restricted
    if ((is_restricted(from) || is_restricted(to)) && !is_bypass_allowed(from) && !is_bypass_allowed(to)) {
        log_action("FAILED", "RENAME", from);
        return -ENOENT;
    }

    // Rename the file or directory
    int res = rename(from, to);
    if (res == 0) {
        log_action("SUCCESS", "RENAME", from);
    } else {
        log_action("FAILED", "RENAME", from);
    }

    return res;
}

static int germa_rmdir(const char *path) {
    // Check if the path is restricted
    if (is_restricted(path) && !is_bypass_allowed(path)) {
        log_action("FAILED", "RMDIR", path);
        return -ENOENT;
    }

    // Remove the directory
    int res = rmdir(path);
    if (res == 0) {
        log_action("SUCCESS", "RMDIR", path);
    } else {
        log_action("FAILED", "RMDIR", path);
    }

    return res;
}

static int germa_unlink(const char *path) {
    // Check if the path is restricted
    if (is_restricted(path) && !is_bypass_allowed(path)) {
        log_action("FAILED", "RMFILE", path);
        return -ENOENT;
    }

    // Remove the file
    int res = unlink(path);
    if (res == 0) {
        log_action("SUCCESS", "RMFILE", path);
    } else {
        log_action("FAILED", "RMFILE", path);
    }

    return res;
}

static struct fuse_operations germa_oper = {
    .getattr = germa_getattr,
    .readdir = germa_readdir,
    .mkdir = germa_mkdir,
    .rename = germa_rename,
    .rmdir = germa_rmdir,
    .unlink = germa_unlink,
};

int main(int argc, char *argv[]) {
    // Mount the FUSE filesystem
    return fuse_main(argc, argv, &germa_oper, NULL);
}
