# sisop-praktikum-modul-4-2023-WS-F07



# Soal 2 

## Deskripsi Soal 
Nana adalah peri kucing yang terampil dalam menggunakan sihir, dia bisa membuat orang lain berubah menjadi Molina. Suatu hari, dia bosan menggunakan sihir dan mencoba untuk magang di germa.dev sebagai programmer.
Agar dapat diterima sebagai karyawan magang dia diberi sebuah file .zip yang berisi folder dan file dari Germa. Kemudian, Nana harus membuat sistem manajemen folder dengan ketentuan sebagai berikut:
Apabila terdapat file yang mengandung kata restricted, maka file tersebut tidak dapat di-rename ataupun dihapus.
Apabila terdapat folder yang mengandung kata restricted, maka folder tersebut, folder yang ada di dalamnya, dan file yang ada di dalamnya tidak dapat di-rename ataupun dihapus.

# code
```
#define FUSE_USE_VERSION 31

#include <fuse.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

static const char *zip_path = "/home/arkandarvesh/zip/nanaxgerma.zip";  // Path to the ZIP file
static const char *log_file = "/home/arkandarvesh/logmucatatsini.txt";  // Path to the log file
static const char *restricted_folder = "/home/arkandarvesh/src_data/germa/projects/restricted_list";
static const char *bypass_folder = "/home/arkandarvesh/src_data/germa/projects/bypass_list";
static const char *restricted_file = "/home/arkandarvesh/src_data/germa/projects/restricted_list/filePenting";
static const char *restricted_file_lama = "/home/arkandarvesh/src_data/germa/projects/restricted_list/restrictedFileLama";
static const char *bypass_keyword = "bypass";

static int is_restricted(const char *path) {
    // Check if the path contains the word "restricted"
    return (strstr(path, "restricted") != NULL);
}

static int is_bypass_allowed(const char *path) {
    // Check if the path contains the bypass keyword
    return (strstr(path, bypass_keyword) != NULL);
}

static void log_action(const char *status, const char *cmd, const char *desc) {
    // Get the current time
    time_t t = time(NULL);
    struct tm tm = *localtime(&t);

    // Open the log file in append mode
    FILE *file = fopen(log_file, "a");
    if (file == NULL) {
        perror("Failed to open log file");
        return;
    }

    // Write the log entry to the file
    fprintf(file, "%s::%02d/%02d/%04d-%02d:%02d:%02d::%s::%s\n",
            status,
            tm.tm_mday, tm.tm_mon + 1, tm.tm_year + 1900,
            tm.tm_hour, tm.tm_min, tm.tm_sec,
            cmd, desc);

    // Close the log file
    fclose(file);
}

static int germa_getattr(const char *path, struct stat *stbuf) {
    // Check if the path is restricted
    if (is_restricted(path) && !is_bypass_allowed(path)) {
        return -ENOENT;
    }

    // Set the permissions and other attributes of the file/directory
    memset(stbuf, 0, sizeof(struct stat));
    if (strcmp(path, "/") == 0) {
        // Root directory
        stbuf->st_mode = S_IFDIR | 0755;
        stbuf->st_nlink = 2;
    } else {
        // Regular file or directory
        stbuf->st_mode = S_IFREG | 0644;
        stbuf->st_nlink = 1;
        stbuf->st_size = 0;
    }

    return 0;
}

static int germa_readdir(const char *path, void *buf, fuse_fill_dir_t filler,
                         off_t offset, struct fuse_file_info *fi) {
    // Check if the path is restricted
    if (is_restricted(path) && !is_bypass_allowed(path)) {
        return -ENOENT;
    }

    // Add the entries to the directory listing
    filler(buf, ".", NULL, 0);
    filler(buf, "..", NULL, 0);
    filler(buf, "file1.txt", NULL, 0);
    filler(buf, "file2.txt", NULL, 0);
    filler(buf, "file3.txt", NULL, 0);

    return 0;
}

static int germa_mkdir(const char *path, mode_t mode) {
    // Check if the path is restricted
    if (is_restricted(path) && !is_bypass_allowed(path)) {
        log_action("FAILED", "MKDIR", path);
        return -ENOENT;
    }

    // Create the directory
    int res = mkdir(path, mode);
    if (res == 0) {
        log_action("SUCCESS", "MKDIR", path);
    } else {
        log_action("FAILED", "MKDIR", path);
    }

    return res;
}

static int germa_rename(const char *from, const char *to) {
    // Check if the source or destination paths are restricted
    if ((is_restricted(from) || is_restricted(to)) && !is_bypass_allowed(from) && !is_bypass_allowed(to)) {
        log_action("FAILED", "RENAME", from);
        return -ENOENT;
    }

    // Rename the file or directory
    int res = rename(from, to);
    if (res == 0) {
        log_action("SUCCESS", "RENAME", from);
    } else {
        log_action("FAILED", "RENAME", from);
    }

    return res;
}

static int germa_rmdir(const char *path) {
    // Check if the path is restricted
    if (is_restricted(path) && !is_bypass_allowed(path)) {
        log_action("FAILED", "RMDIR", path);
        return -ENOENT;
    }

    // Remove the directory
    int res = rmdir(path);
    if (res == 0) {
        log_action("SUCCESS", "RMDIR", path);
    } else {
        log_action("FAILED", "RMDIR", path);
    }

    return res;
}

static int germa_unlink(const char *path) {
    // Check if the path is restricted
    if (is_restricted(path) && !is_bypass_allowed(path)) {
        log_action("FAILED", "RMFILE", path);
        return -ENOENT;
    }

    // Remove the file
    int res = unlink(path);
    if (res == 0) {
        log_action("SUCCESS", "RMFILE", path);
    } else {
        log_action("FAILED", "RMFILE", path);
    }

    return res;
}

static struct fuse_operations germa_oper = {
    .getattr = germa_getattr,
    .readdir = germa_readdir,
    .mkdir = germa_mkdir,
    .rename = germa_rename,
    .rmdir = germa_rmdir,
    .unlink = germa_unlink,
};

int main(int argc, char *argv[]) {
    // Mount the FUSE filesystem
    return fuse_main(argc, argv, &germa_oper, NULL);
}
```
## Penjelasan
```
#define FUSE_USE_VERSION 31
```
Ini mendefinisikan versi FUSE yang akan digunakan. Di sini, kita menggunakan versi FUSE 31.

```
static const char *zip_path = "/home/arkandarvesh/zip/nanaxgerma.zip";  // Path to the ZIP file
static const char *log_file = "/home/arkandarvesh/logmucatatsini.txt";  // Path to the log file
static const char *restricted_folder = "/home/arkandarvesh/src_data/germa/projects/restricted_list";
static const char *bypass_folder = "/home/arkandarvesh/src_data/germa/projects/bypass_list";
static const char *restricted_file = "/home/arkandarvesh/src_data/germa/projects/restricted_list/filePenting";
static const char *restricted_file_lama = "/home/arkandarvesh/src_data/germa/projects/restricted_list/restrictedFileLama";
static const char *bypass_keyword = "bypass";
```
Bagian ini mendeklarasikan beberapa variabel konstanta yang berisi path file atau folder yang akan digunakan dalam program.

```
static int is_restricted(const char *path) {
    // Check if the path contains the word "restricted"
    return (strstr(path, "restricted") != NULL);
}

static int is_bypass_allowed(const char *path) {
    // Check if the path contains the bypass keyword
    return (strstr(path, bypass_keyword) != NULL);
}

static void log_action(const char *status, const char *cmd, const char *desc) {
    // Get the current time
    time_t t = time(NULL);
    struct tm tm = *localtime(&t);

    // Open the log file in append mode
    FILE *file = fopen(log_file, "a");
    if (file == NULL) {
        perror("Failed to open log file");
        return;
    }

    // Write the log entry to the file
    fprintf(file, "%s::%02d/%02d/%04d-%02d:%02d:%02d::%s::%s\n",
            status,
            tm.tm_mday, tm.tm_mon + 1, tm.tm_year + 1900,
            tm.tm_hour, tm.tm_min, tm.tm_sec,
            cmd, desc);

    // Close the log file
    fclose(file);
}
```
Bagian ini berisi definisi fungsi-fungsi pendukung yang digunakan dalam program utama. Fungsi is_restricted digunakan untuk memeriksa apakah suatu path mengandung kata "restricted". Fungsi is_bypass_allowed digunakan untuk memeriksa apakah suatu path mengandung kata kunci "bypass". Fungsi log_action digunakan untuk mencatat tindakan ke file log, termasuk status, perintah, dan deskripsi tindakan tersebut.

```
static int germa_getattr(const char *path, struct stat *stbuf) {
    // Check if the path is restricted
    if (is_restricted(path) && !is_bypass_allowed(path)) {
        return -ENOENT;
    }

    // Set the permissions and other attributes of the file/directory
    memset(stbuf, 0, sizeof(struct stat));
    if (strcmp(path, "/") == 0) {
        // Root directory
        stbuf->st_mode = S_IFDIR | 0755;
        stbuf->st_nlink = 2;
    } else {
        // Regular file or directory
        stbuf->st_mode = S_IFREG | 0644;
        stbuf->st_nlink = 1;
        stbuf->st_size = 0;
    }

    return 0;
}

static int germa_readdir(const char *path, void *buf, fuse_fill_dir_t filler,
                         off_t offset, struct fuse_file_info *fi) {
    // Check if the path is restricted
    if (is_restricted(path) && !is_bypass_allowed(path)) {
        return -ENOENT;
    }

    // Add the entries to the directory listing
    filler(buf, ".", NULL, 0);
    filler(buf, "..", NULL, 0);
    filler(buf, "file1.txt", NULL, 0);
    filler(buf, "file2.txt", NULL, 0);
    filler(buf, "file3.txt", NULL, 0);

    return 0;
}

static int germa_mkdir(const char *path, mode_t mode) {
    // Check if the path is restricted
    if (is_restricted(path) && !is_bypass_allowed(path)) {
        log_action("FAILED", "MKDIR", path);
        return -ENOENT;
    }

    // Create the directory
    int res = mkdir(path, mode);
    if (res == 0) {
        log_action("SUCCESS", "MKDIR", path);
    } else {
        log_action("FAILED", "MKDIR", path);
    }

    return res;
}

static int germa_rename(const char *from, const char *to) {
    // Check if the source or destination paths are restricted
    if ((is_restricted(from) || is_restricted(to)) && !is_bypass_allowed(from) && !is_bypass_allowed(to)) {
        log_action("FAILED", "RENAME", from);
        return -ENOENT;
    }

    // Rename the file or directory
    int res = rename(from, to);
    if (res == 0) {
        log_action("SUCCESS", "RENAME", from);
    } else {
        log_action("FAILED", "RENAME", from);
    }

    return res;
}

static int germa_rmdir(const char *path) {
    // Check if the path is restricted
    if (is_restricted(path) && !is_bypass_allowed(path)) {
        log_action("FAILED", "RMDIR", path);
        return -ENOENT;
    }

    // Remove the directory
    int res = rmdir(path);
    if (res == 0) {
        log_action("SUCCESS", "RMDIR", path);
    } else {
        log_action("FAILED", "RMDIR", path);
    }

    return res;
}

static int germa_unlink(const char *path) {
    // Check if the path is restricted
    if (is_restricted(path) && !is_bypass_allowed(path)) {
        log_action("FAILED", "RMFILE", path);
        return -ENOENT;
    }

    // Remove the file
    int res = unlink(path);
    if (res == 0) {
        log_action("SUCCESS", "RMFILE", path);
    } else {
        log_action("FAILED", "RMFILE", path);
    }

    return res;
}
```
Bagian ini berisi implementasi fungsi-fungsi operasi FUSE yang akan dipanggil oleh FUSE saat ada operasi yang sesuai. Beberapa operasi yang diimplementasikan antara lain:

    germa_getattr: Mengambil atribut (permissions, size, dll) dari file atau direktori yang ditentukan oleh path.
    germa_readdir: Membaca isi direktori yang ditentukan oleh path dan mengisi buf dengan entri-entri yang ada.
    germa_mkdir: Membuat direktori baru dengan path dan mode (hak akses) yang ditentukan.
    germa_rename: Mengganti nama file atau direktori dari from menjadi to.
    germa_rmdir: Menghapus direktori yang ditentukan oleh path.
    germa_unlink: Menghapus file yang ditentukan oleh path.

```
static struct fuse_operations germa_oper = {
    .getattr = germa_getattr,
    .readdir = germa_readdir,
    .mkdir = germa_mkdir,
    .rename = germa_rename,
    .rmdir = germa_rmdir,
    .unlink = germa_unlink,
};
```
Bagian ini mendefinisikan struktur germa_oper yang berisi fungsi-fungsi operasi FUSE yang telah diimplementasikan sebelumnya.
```
int main(int argc, char *argv[]) {
    // Mount the FUSE filesystem
    return fuse_main(argc, argv, &germa_oper, NULL);
}
```
    Bagian ini merupakan fungsi main yang merupakan titik masuk utama program. Fungsi ini akan memanggil fuse_main untuk memulai dan mengatur sistem file FUSE dengan menggunakan operasi yang telah didefinisikan dalam germa_oper.

Dengan demikian, program ini mengimplementasikan sistem file FUSE yang memungkinkan pengguna untuk mengakses file dan direktori dengan beberapa pembatasan yang ditentukan. Program juga mencatat tindakan-tindakan yang dilakukan ke dalam file log.

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/sisop-praktikum-modul-1-2023-ws-f07/sisop-praktikum-modul-4-2023-ws-f07/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
